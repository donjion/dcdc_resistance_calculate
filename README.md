适用于大部分基于反馈控制的 DC-DC 转换器。

输入芯片的反馈电压（Vfb）值，和预期的输出电压值（Vout），以及允许的误差范围和输出组数，程序会自动计算出符合要求的R1、R2电阻建议。计算时只使用常见的1%精度贴片电阻取值（例如220Ω、4.7k、5.1k、10k等，取值参考嘉立创SMT基础库），并且最大串联数不超过2。

打包命令：pyinstaller --onefile dcdc_calculate.py
